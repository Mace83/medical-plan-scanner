export interface MedialPlanAttributes {
  MP?: {
    '@attributes': {
      U: string;
      l: 'de-DE' // language
      v: string;
    },
    A: { // doctor
      '@attributes': {
        c: string; // city
        e: string; // email
        n: string; // name
        p: string;
        s: string; // street
        t: string; // time
        z: string;
      }
    },
    P: { // Patient
      '@attributes': {
        b: string;
        f: string; // lastname
        g: string; // forename
      }
    },
    S: [
      MedicalPart,
      SpecialTimeMedical,
      SelfMedication
    ]
  };
}

export interface SpecialTimeMedical {
  attributes: {
    t: string; // title
  };
  M: MedicalPart;
}

export interface SelfMedication {
  attributes: {
    c: string;
  };
  M: MedicalPart;
}

export interface MedicalPart {
  M: {
    '@attributes': {
      du: string;
      m: string;
      p: string;
      r: string;
      i: string;
      v: string;
    };
  };
}

// Todo noon and night
export interface Medications {
  agent: string;
  reason: string;
  toTake: {
    morning: string;
    evening: string;
  };
}
