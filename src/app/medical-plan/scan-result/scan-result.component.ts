import {Component, Input, OnInit} from '@angular/core';
import { NgxXml2jsonService } from 'ngx-xml2json';
import {MedialPlanAttributes, Medications} from '../../interfaces/medication.interfaces';


@Component({
  selector: 'app-scan-result',
  templateUrl: './scan-result.component.html',
  styleUrls: ['./scan-result.component.scss']
})
export class ScanResultComponent implements OnInit {

  public isDataScanned = false;
  public medications: Array<Medications> = [];

  private _scannedData: string = '';
  get scannedData(): string {
    return this._scannedData;
  }

  @Input() set scannedData(value: string) {
    this._scannedData = value;
    this.onDataScanned(value);
  }

  constructor(private xmlToJsonService: NgxXml2jsonService) {
  }

  ngOnInit(): void {
  }

  public onDataScanned(data: string): void {
    const parser = new DOMParser();
    const xml = parser.parseFromString(data, 'text/xml');
    const json: MedialPlanAttributes = this.xmlToJsonService.xmlToJson(xml);

    this.convertXmlJsonDataToTableData(json.MP);

    this.isDataScanned = true;
  }

  private convertXmlJsonDataToTableData(medicalPlan: any): void {
    for (const med of medicalPlan?.S[0]?.M) {
      this.medications.push(
        {
          agent: med['@attributes'].p,
          reason: med['@attributes'].r,
          toTake: {
            morning: med['@attributes'].m,
            evening: med['@attributes'].v
          }
        }
      );
    }
  }


}
