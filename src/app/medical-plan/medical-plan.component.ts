import {Component} from '@angular/core';


@Component({
  selector: 'app-medical-plan',
  templateUrl: './medical-plan.component.html',
  styleUrls: ['./medical-plan.component.scss']
})
export class MedicalPlanComponent {
  public scannedData = '';
  public isStartClicked = false;

  constructor() {
  }
}
