import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalPlanComponent } from './medical-plan.component';

describe('MedicalPlanComponent', () => {
  let component: MedicalPlanComponent;
  let fixture: ComponentFixture<MedicalPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
