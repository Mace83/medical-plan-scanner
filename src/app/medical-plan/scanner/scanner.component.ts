import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BarcodeFormat } from '@zxing/library';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {
  public enabledBarCodeFormats: BarcodeFormat[] = [
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.CODE_128,
    BarcodeFormat.AZTEC,
    BarcodeFormat.MAXICODE,
  ];

  @Output() hasCameraPermissions: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() scannedData: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  public onScanSuccess(document: string): void {
    if (!document) {
      return;
    }
    this.scannedData.emit(document);
  }

  // not used currently
  public onHasPermission(value: boolean): void {
    this.hasCameraPermissions.emit(value);
  }
}
