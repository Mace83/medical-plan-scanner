import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MedicalPlanComponent } from './medical-plan/medical-plan.component';

const routes: Routes = [
  {
    path: '',
    component: MedicalPlanComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
